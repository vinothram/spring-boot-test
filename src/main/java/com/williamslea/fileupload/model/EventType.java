package com.williamslea.fileupload.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity

public class EventType {
	
	@Id
	private String eventCode;
	@Lob
	private String eventDescription;
	
	public EventType(String eventCode, String eventDescription) {
		this.eventCode = eventCode;
		this.eventDescription = eventDescription;
	}
	public EventType() {
		// TODO Auto-generated constructor stub
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
	
}
