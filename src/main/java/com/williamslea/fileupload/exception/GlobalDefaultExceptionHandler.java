package com.williamslea.fileupload.exception;

import java.io.IOException;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalDefaultExceptionHandler {

	public static final String ERROR_VIEW = "customerror";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);
	
	@ExceptionHandler(value = Exception.class)
	  public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
		return new ResponseEntity<String>("Exception occured, please contact administrator", HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler(value = RuntimeException.class)
	  public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, RuntimeException e) throws Exception {
		return new ResponseEntity<String>("RuntimeException occured, please contact administrator", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	@ExceptionHandler(value = NoSuchElementException.class)
	  public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, NoSuchElementException e) throws Exception {
		return new ResponseEntity<String>("NoSuchElementException occured, please contact administrator", HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(value = StringIndexOutOfBoundsException.class)
	  public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, StringIndexOutOfBoundsException e) throws Exception {
		return new ResponseEntity<String>("StringIndexOutOfBoundsException occured, please contact administrator", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	@ExceptionHandler(value = IOException.class)
	  public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, IOException e) throws Exception {
		return new ResponseEntity<String>("IOException occured, please contact administrator", HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
