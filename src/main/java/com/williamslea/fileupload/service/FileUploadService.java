package com.williamslea.fileupload.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.williamslea.fileupload.model.CompanyDetails;
import com.williamslea.fileupload.model.EventType;
import com.williamslea.fileupload.repository.CompanyRepository;
import com.williamslea.fileupload.repository.EventRepository;

/**
 * This class reads the text file being uploaded from the end point and inserts
 * the records in the database
 * 
 * @author Sangeetha
 *
 */
@Service
public class FileUploadService {

	@Autowired
	private EventRepository eventRepository;
	@Autowired
	private CompanyRepository companyRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadService.class);

	@Value("${textfile.startindex}")
	private int startIndex;
	@Value("${eventcode.endindex}")
	private int eventCodeEndIndex;
	@Value("${companyname.endindex}")
	private int companyNameEndIndex;
	@Value("${companynumber.endindex}")
	private int companyNumberEndIndex;
	@Value("${eventtype.endindex}")
	private int eventTypeEndIndex;
	@Value("${eventdate.endindex}")
	private int eventDateEndIndex;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	/* Pattern to extract the event codes within () */
	Pattern pattern = Pattern.compile("\\((.*?)\\)");

	/**
	 * This method will insert the records into H2 Database
	 * 
	 * @param multiFile Text file being uploaded from the end point
	 */
	public void uploadToDB(MultipartFile multiFile) throws IOException{
		LOGGER.info("Going to upload in Database");
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(multiFile.getInputStream()));
			ArrayList<String> eventsFromFile = new ArrayList<String>();
			List<EventType> eventTypeList = new ArrayList<EventType>();
			List<CompanyDetails> finalCompanyList = new ArrayList<CompanyDetails>();
			String currentLine = "";
			int i = 0;
			// While loop to read the lines containing event strings until ******* and save
			// it to the list
			while ((currentLine = reader.readLine()) != null) {
				if (currentLine.contains("****************************"))
					break;
				// To avoid lines containing only empty spaces and two texts LOUISE SMYTH and
				// REGISTRAR OF COMPANIES are not part of event or company information
				else if (!currentLine.trim().equals("") && !currentLine.trim().equals("LOUISE SMYTH")
						&& !currentLine.trim().equals("REGISTRAR OF COMPANIES")) {
					eventsFromFile.add(currentLine);
				}
			}
			eventTypeList = updateEventRecords(i, eventsFromFile, eventTypeList);
			eventRepository.saveAll(eventTypeList);
			LOGGER.info("Event records are updated");

			ArrayList<String> companyInfoFromFile = new ArrayList<String>();
			// While loop to read the lines containing companies information
			while ((currentLine = reader.readLine()) != null) {
				if (!currentLine.trim().equals("")) {
					companyInfoFromFile.add(currentLine);
				}
			}
			reader.close();
			i = 0;
			finalCompanyList = updateCompanyRecords(i, companyInfoFromFile, finalCompanyList);
			companyRepository.saveAll(finalCompanyList);
			LOGGER.info("Company records are updated");
		} catch (IOException e) {
			LOGGER.error(e.toString());
			e.printStackTrace();
		}

	}

	/**
	 * This method retrieves the company details from a line in the text file and
	 * adds the retrieved value to the list.
	 * 
	 * @param i                integer to loop through the list
	 * @param companyList      List of strings retrieved from the text file
	 * @param finalCompanyList List to add the retrieved company information
	 *                         containing company name, company number, event
	 *                         description, event date and unique identifier.
	 * @return List of Company with details
	 */
	private List<CompanyDetails> updateCompanyRecords(int i, ArrayList<String> companyList,
			List<CompanyDetails> finalCompanyList) {
		/*
		 * Assumed this as a fixed length strings with company name, company number,
		 * event code, event date.
		 */
		/* Index 0 to 47 has company name */
		/* Index 47 to 59 has company number */
		/* Index 59 to 64 has event code */
		/* Index 64 till end of line has event date */
		CompanyDetails companyDetails = new CompanyDetails();
		for (String company : companyList) {
			i = i + 1;
			String companyName = "";
			String companyNumber = "";
			if (company.length() > companyNameEndIndex) {
				companyNumber = company.substring(companyNameEndIndex, companyNumberEndIndex);
			}
			String eventTyp = "";
			String eventDate = "";
			/*
			 * Few lines contain empty company number which implies that the company name is
			 * a continuation of the previous line
			 */
			if (companyNumber.trim().equals("")) {
				String prevLine = companyList.get(i - 2);
				String prevCompName = prevLine.substring(startIndex, companyNameEndIndex);
				/* There are few scenarios where company name is wrapped to third line also */
				if (prevLine.substring(companyNameEndIndex, companyNumberEndIndex).trim().equals("")) {
					String secondPrevLine = companyList.get(i - 3);
					prevCompName = secondPrevLine.substring(startIndex, companyNameEndIndex).trim() + " "
							+ prevCompName.trim() + companyName.trim();
					companyName = prevCompName;
					companyNumber = secondPrevLine.substring(companyNameEndIndex, companyNumberEndIndex);
					eventTyp = secondPrevLine.substring(companyNumberEndIndex, eventTypeEndIndex);
					Matcher matcher = pattern.matcher(eventTyp);
					while (matcher.find()) {
						eventTyp = matcher.group(1);
					}
					eventDate = secondPrevLine.substring(eventTypeEndIndex, eventDateEndIndex);
				} else {
					companyName = company.substring(startIndex, companyNameEndIndex);
					companyName = prevCompName.trim() + " " + companyName.trim();
					companyNumber = prevLine.substring(companyNameEndIndex, companyNumberEndIndex);
					eventTyp = prevLine.substring(companyNumberEndIndex, eventTypeEndIndex);
					Matcher matcher = pattern.matcher(eventTyp);
					while (matcher.find()) {
						eventTyp = matcher.group(1);
					}
					eventDate = prevLine.substring(eventTypeEndIndex, eventDateEndIndex);
				}

			} else {
				companyDetails = new CompanyDetails();
				companyName = company.substring(startIndex, companyNameEndIndex);
				companyNumber = company.substring(companyNameEndIndex, companyNumberEndIndex);
				eventTyp = company.substring(companyNumberEndIndex, eventTypeEndIndex);
				Matcher matcher = pattern.matcher(eventTyp);
				while (matcher.find()) {
					eventTyp = matcher.group(1);
				}
				eventDate = company.substring(eventTypeEndIndex, eventDateEndIndex);
			}
			companyDetails.setCompanyName(companyName.trim());
			companyDetails.setCompanyNumber(companyNumber.trim());
			String eventDescription = "";
			/*
			 * To update the company table with event description equivalent to event code
			 */
			eventTyp = "S2";
			eventDescription = eventRepository.findById(eventTyp).get().getEventDescription();
			/*try {
				
			} catch (Exception e) {
				
				LOGGER.warn("Company Number " + companyNumber.trim() + " doesn't have event code");
			} */
			companyDetails.setEventType(eventDescription);
			companyDetails.setEventDate(eventDate.trim());
			finalCompanyList.add(companyDetails);

		}

		return finalCompanyList;
	}

	/**
	 * This method retrieves the event code and event description from a line in the
	 * text file and adds the retrieved value to the list.
	 * 
	 * @param i              integer to loop through the list
	 * @param eventsFromFile List of strings retrieved from the text file
	 * @param eventTypeList  List to add the retrieved events with event code and
	 *                       description
	 * @return List of EventType
	 */
	private List<EventType> updateEventRecords(int i, ArrayList<String> eventsFromFile, List<EventType> eventTypeList) {
		/*
		 * Assumed this as a fixed length strings with event code and event description.
		 */
		/* Index 0 to 27 has event code and is referred from application properties */
		/* Index 27 to end of line has event description */

		for (String lineNow : eventsFromFile) {
			i = i + 1;
			String eventCode = "";
			String eventDescription = "";
			 eventCode = lineNow.substring(startIndex, eventCodeEndIndex);
			 //eventCode = lineNow.substring(-5, 0);
			Matcher matcher = pattern.matcher(eventCode);
			EventType eventObject = new EventType();
			while (matcher.find()) {
				eventCode = matcher.group(1);
			}
			eventDescription = lineNow.substring(eventCodeEndIndex, lineNow.length());
			boolean hasNextEventCode = false;
			/*
			 * Few lines contain empty event code which implies that the company name is a
			 * continuation of the previous line
			 */
			if (eventCode.trim().equals("")) {
				hasNextEventCode = true;
				String prevLine = eventsFromFile.get(i - 2);
				eventCode = prevLine.substring(startIndex, eventCodeEndIndex);
				String secondPrevDesc = lineNow.substring(eventCodeEndIndex, lineNow.length());
				/* There are few scenarios where event name is wrapped to third line also */
				if (eventCode.trim().equals("")) {
					String thirdPrevLine = eventsFromFile.get(i - 3);
					String thirdPrevDesc = thirdPrevLine.substring(eventCodeEndIndex, thirdPrevLine.length());
					eventCode = thirdPrevLine.substring(startIndex, eventCodeEndIndex);
					Matcher mat = pattern.matcher(eventCode);
					while (mat.find()) {
						eventCode = mat.group(1);
					}
					eventDescription = thirdPrevDesc.trim() + " " + prevLine.trim() + " " + secondPrevDesc.trim();
				} else {
					Matcher mat = pattern.matcher(eventCode);
					while (mat.find()) {
						eventCode = mat.group(1);
					}
					prevLine = prevLine.substring(eventCodeEndIndex, prevLine.length());
					eventDescription = prevLine + " " + eventDescription;
				}
			} else {
				eventObject = new EventType();
				if (hasNextEventCode) {
					eventDescription = eventDescription.trim() + eventsFromFile.get(i - 1).trim();
				} else {
					eventDescription = eventDescription.trim();
				}
			}
			eventObject.setEventCode(eventCode.trim());
			eventObject.setEventDescription(eventDescription.trim());
			eventTypeList.add(eventObject);
		}
		return eventTypeList;
	}

}
