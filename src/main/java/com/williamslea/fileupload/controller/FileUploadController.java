package com.williamslea.fileupload.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.williamslea.fileupload.service.FileUploadService;

/**
 * Controller class to process get and post requests
 * 
 * @author Sangeetha
 *
 */
@Controller
public class FileUploadController {

	@Autowired
	private FileUploadService fileUploadService;

	private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);

	/**
	 * This method handles the get request
	 * 
	 * @param model Model
	 * @return string which is a html page
	 * @throws IOException
	 */
	@GetMapping("/")
	public String listUploadedFiles(Model model) throws IOException {
		return "uploadForm";
	}

	/**
	 * This method process the post request and file gets uploaded to the Database
	 * 
	 * @param multiPartFile      MultipartFile
	 * @param redirectAttributes RedirectAttributes
	 * @return string which is a H2 console
	 * @throws Exception
	 */
	@PostMapping("/")
	public String uploadToDB(@RequestParam("file") MultipartFile multiPartFile) throws Exception {
			fileUploadService.uploadToDB(multiPartFile);
			return "redirect:/h2-console";
	}
	

}
