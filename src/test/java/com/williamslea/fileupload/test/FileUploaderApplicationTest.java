package com.williamslea.fileupload.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.williamslea.fileupload.controller.FileUploadController;
import com.williamslea.fileupload.repository.EventRepository;

@RunWith(SpringRunner.class)
@SpringBootTest

public class FileUploaderApplicationTest {

	@Autowired
	private FileUploadController controller;

	@Autowired
	private EventRepository eventRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(FileUploaderApplicationTest.class);

	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}
}
